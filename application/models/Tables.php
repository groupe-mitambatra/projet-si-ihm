<?php 
   class Tables extends CI_Model {
        private $idTables
        private $numero


        /**
         * Get the value of idTables
         */ 
        public function getIdTables()
        {
                return $this->idTables;
        }

        /**
         * Set the value of idTables
         *
         * @return  self
         */ 
        public function setIdTables($idTables)
        {
                $this->idTables = $idTables;

                return $this;
        }

        /**
         * Get the value of numero
         */ 
        public function getNumero()
        {
                return $this->numero;
        }

        /**
         * Set the value of numero
         *
         * @return  self
         */ 
        public function setNumero($numero)
        {
                $this->numero = $numero;

                return $this;
        }
    } 
?>