<?php
	class Plat extends CI_Model{
		public $idPlat;
		public $idCategorie;
		public $nom;
		public $prix;
		public $img;

		public function selectPlat($nom=FALSE){
			$result=array();
			$query=$this->db->get('Plat');
			foreach($query->result() as $plats){
				$plat= new Plat();
				$plat->setIdPlat($plats->idPlat);
				$plat->setNom($plats->nom);
				$plat->setCategorie($plats->Categorie);
				$plat->setImage($plats->img);
				$plat->setPrix($plats->prix);
				$result[]=$plat;
			}
			return $result;
		}

		/**
		 * Get the value of idPlat
		 */ 
		public function getIdPlat()
		{
				return $this->idPlat;
		}

		/**
		 * Set the value of idPlat
		 *
		 * @return  self
		 */ 
		public function setIdPlat($idPlat)
		{
				$this->idPlat = $idPlat;

				return $this;
		}

		/**
		 * Get the value of nom
		 */ 
		public function getNom()
		{
				return $this->nom;
		}

		/**
		 * Set the value of nom
		 *
		 * @return  self
		 */ 
		public function setNom($nom)
		{
				$this->nom = $nom;

				return $this;
		}

		/**
		 * Get the value of categorie
		 */ 
		public function getCategorie()
		{
				return $this->idCcategorie;
		}

		/**
		 * Set the value of categorie
		 *
		 * @return  self
		 */ 
		public function setCategorie($idCategorie)
		{
				$this->categorie = $idCategorie;

				return $this;
		}

		/**
		 * Get the value of image
		 */ 
		public function getImg()
		{
				return $this->img;
		}

		/**
		 * Set the value of image
		 *
		 * @return  self
		 */ 
		public function setImg($img)
		{
				$this->img = $img;

				return $this;
		}

		/**
		 * Get the value of prix
		 */ 
		public function getPrix()
		{
				return $this->prix;
		}

		/**
		 * Set the value of prix
		 *
		 * @return  self
		 */ 
		public function setPrix($prix)
		{
				$this->prix = $prix;

				return $this;
		}
	}
?>
