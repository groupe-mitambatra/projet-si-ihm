<?php 
   class Categorie extends CI_Model {
       private $idCommande
       private $idPlat
       private $nombre


       /**
        * Get the value of idCommande
        */ 
       public function getIdCommande()
       {
              return $this->idCommande;
       }

       /**
        * Set the value of idCommande
        *
        * @return  self
        */ 
       public function setIdCommande($idCommande)
       {
              $this->idCommande = $idCommande;

              return $this;
       }

       /**
        * Get the value of idPlat
        */ 
       public function getIdPlat()
       {
              return $this->idPlat;
       }

       /**
        * Set the value of idPlat
        *
        * @return  self
        */ 
       public function setIdPlat($idPlat)
       {
              $this->idPlat = $idPlat;

              return $this;
       }

       /**
        * Get the value of nombre
        */ 
       public function getNombre()
       {
              return $this->nombre;
       }

       /**
        * Set the value of nombre
        *
        * @return  self
        */ 
       public function setNombre($nombre)
       {
              $this->nombre = $nombre;

              return $this;
       }
   }
?>