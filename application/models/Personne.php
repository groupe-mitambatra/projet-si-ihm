<?php 
   class Personne extends CI_Model {
        private $idPersonne;
        private $nom;
        private $types;
        private $mdp;
       
       public function __construct(){
               parent::__construct();
                $this->load->database();
       }
       
       
       
        /**
         * Get the value of idPersonne
         */ 
        public function getIdPersonne()
        {
                return $this->idPersonne;
        }

        /**
         * Set the value of idPersonne
         *
         * @return  self
         */ 
        public function setIdPersonne($idPersonne)
        {
                $this->idPersonne = $idPersonne;

                return $this;
        }

        /**
         * Get the value of nom
         */ 
        public function getNom()
        {
                return $this->nom;
        }

        /**
         * Set the value of nom
         *
         * @return  self
         */ 
        public function setNom($nom)
        {
                $this->nom = $nom;

                return $this;
        }

        /**
         * Get the value of types
         */ 
        public function getTypes()
        {
                return $this->types;
        }

        /**
         * Set the value of types
         *
         * @return  self
         */ 
        public function setTypes($types)
        {
                $this->types = $types;

                return $this;
        }

        /**
         * Get the value of mdp
         */ 
        public function getMdp()
        {
                return $this->mdp;
        }

        /**
         * Set the value of mdp
         *
         * @return  self
         */ 
        public function setMdp($mdp)
        {
                $this->mdp = $mdp;

                return $this;
        }
        public function login($username,$mdp){
                $resultat=array();
                    $count=0;
                    $query=$this->db->get_where('Personne',array('nom'=>$username));
                    foreach($query->result() as $user){
                            $count+=1;
                            $found=false;
                            if(strcmp($user->nom,$username)==0 && strcmp($mdp,$user->mdp)==0){
                                    $found=true;
                            }
                            if(!$found){
                                // $resultat['errorPassword']=true;
                            }
                            else{
                                $resultat['user']=array(
                                        'idPersonne'=>$user->IDPERSONNE,
                                        'type'=>$user->TYPE
                                );
                            }
                    }
                    if($count==0){
                            $resultat['errorLogin']=true;
                    }
                    return $resultat;
            }
    } 
?>