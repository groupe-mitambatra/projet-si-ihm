<?php 
   class Facture extends CI_Model {
        private $idFacture
        private $idCommande
        private $sommme
        private $idTables

        /**
         * Get the value of idFacture
         */ 
        public function getIdFacture()
        {
                return $this->idFacture;
        }

        /**
         * Set the value of idFacture
         *
         * @return  self
         */ 
        public function setIdFacture($idFacture)
        {
                $this->idFacture = $idFacture;

                return $this;
        }

        /**
         * Get the value of idCommande
         */ 
        public function getIdCommande()
        {
                return $this->idCommande;
        }

        /**
         * Set the value of idCommande
         *
         * @return  self
         */ 
        public function setIdCommande($idCommande)
        {
                $this->idCommande = $idCommande;

                return $this;
        }

        /**
         * Get the value of sommme
         */ 
        public function getSommme()
        {
                return $this->sommme;
        }

        /**
         * Set the value of sommme
         *
         * @return  self
         */ 
        public function setSommme($sommme)
        {
                $this->sommme = $sommme;

                return $this;
        }

        /**
         * Get the value of idTables
         */ 
        public function getIdTables()
        {
                return $this->idTables;
        }

        /**
         * Set the value of idTables
         *
         * @return  self
         */ 
        public function setIdTables($idTables)
        {
                $this->idTables = $idTables;

                return $this;
        }
   }
?>