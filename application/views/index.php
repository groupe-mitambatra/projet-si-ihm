<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Food</title>
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
	<!-- Bootstrap core CSS -->
	<link href="<?php echo base_url(); ?><?php echo base_url(); ?>mdb/css/bootstrap.min.css" rel="stylesheet">
	<!-- Material Design Bootstrap -->
	<link href="<?php echo base_url(); ?><?php echo base_url(); ?>mdb/css/mdb.min.css" rel="stylesheet">
	<!-- Your custom styles (optional) -->
	<link href="<?php echo base_url(); ?><?php echo base_url(); ?>mdb/css/style.css" rel="stylesheet">
</head>
<body>
	<!-- Default form register -->
<form class="text-center border border-light p-5" action="<?php echo base_url(); ?>index.php/Pers_controller/" method="POST">
<p> <?php if(isset($error)){ echo $error ; } ?> </p>
<p class="h4 mb-4">Connectez vous</p>

<!-- E-mail -->
<input type="text" id="Username" name="username" class="form-control mb-4" placeholder="User Name">

<!-- Password -->
<input type="password" id="mdp"  name="mdp" class="form-control" placeholder="Password" aria-describedby="defaultRegisterFormPasswordHelpBlock">

<!-- Sign up button -->
<button class="btn btn-info my-4 btn-block" type="submit">Sign in</button>

<!-- Social register -->
<p>or sign up with:</p>

<a type="button" class="light-blue-text mx-2">
	<i class="fab fa-facebook-f"></i>
</a>
<a type="button" class="light-blue-text mx-2">
	<i class="fab fa-twitter"></i>
</a>
<a type="button" class="light-blue-text mx-2">
	<i class="fab fa-linkedin-in"></i>
</a>
<a type="button" class="light-blue-text mx-2">
	<i class="fab fa-github"></i>
</a>

<hr>

<!-- Terms of service -->
<p>By clicking
	<em>Sign up</em> you agree to our
	<a href="" target="_blank">terms of service</a>

</form>
<!-- Default form register -->

 <!-- SCRIPTS -->
  <!-- JQuery -->
  <script type="text/javascript" src="<?php echo base_url(); ?><?php echo base_url(); ?><?php echo base_url(); ?>mdb/js/jquery-3.4.1.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="<?php echo base_url(); ?><?php echo base_url(); ?><?php echo base_url(); ?>mdb/js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="<?php echo base_url(); ?><?php echo base_url(); ?><?php echo base_url(); ?>mdb/js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="<?php echo base_url(); ?><?php echo base_url(); ?><?php echo base_url(); ?>mdb/js/mdb.min.js"></script>
</body>
</html>