<?php 
   class Stud_Model extends CI_Model {
      private $idFacture
      private $date
      private $idCommande
      private $somme
      private $idTable

      /**
       * Get the value of idFacture
       */ 
      public function getIdFacture()
      {
            return $this->idFacture;
      }

      /**
       * Set the value of idFacture
       *
       * @return  self
       */ 
      public function setIdFacture($idFacture)
      {
            $this->idFacture = $idFacture;

            return $this;
      }

      /**
       * Get the value of date
       */ 
      public function getDate()
      {
            return $this->date;
      }

      /**
       * Set the value of date
       *
       * @return  self
       */ 
      public function setDate($date)
      {
            $this->date = $date;

            return $this;
      }

      /**
       * Get the value of idCommande
       */ 
      public function getIdCommande()
      {
            return $this->idCommande;
      }

      /**
       * Set the value of idCommande
       *
       * @return  self
       */ 
      public function setIdCommande($idCommande)
      {
            $this->idCommande = $idCommande;

            return $this;
      }

      /**
       * Get the value of somme
       */ 
      public function getSomme()
      {
            return $this->somme;
      }

      /**
       * Set the value of somme
       *
       * @return  self
       */ 
      public function setSomme($somme)
      {
            $this->somme = $somme;

            return $this;
      }

      /**
       * Get the value of idTable
       */ 
      public function getIdTable()
      {
            return $this->idTable;
      }

      /**
       * Set the value of idTable
       *
       * @return  self
       */ 
      public function setIdTable($idTable)
      {
            $this->idTable = $idTable;

            return $this;
      }
   } 
?>