<?php 
   class Pers_controller extends CI_Controller {
	
      function __construct() { 
         parent::__construct(); 
         $this->load->helper('url'); 
         $this->load->database(); 
      } 
  
      public function index()
	{
		$this->load->model('Personne');
        
        $data = array( 
            'username' => $this->input->post('username'), 
            'password' => $this->input->post('mdp') 
            ); 
            
        $resultat=$this->Personne->login($data['username'],$data['password']); 
        if(isset($resultat['errorLogin'])){
            $data['error'] = " Username erroné ";
            $this->load->view('index',$data); 
        }
        if(isset($resultat['errorPassword'])){
            $data['error'] = " Mot de passe erroné ";
            // mbola tsy mety
            $this->load->view('index',$data); 
        }

        if(isset($resultat['user'])){
            redirect('Plat_controller','LesPlatDuJour'); 
        }

    }
    
  
   } 
?>