<?php 
   class Plat_controller extends CI_Controller {
	
      function __construct() { 
         parent::__construct(); 
         $this->load->helper('url'); 
         $this->load->database(); 
      } 

      
      public function LesPlatDuJour() { 

        $today = date("y/m/d");
        $this->load->model('Plat');
        $data['plats']=$this->Plat->platsDuJour($today);
        $data['page']="ListeDesPlats";
        $this->load->view('accueil.php',$data); 
         
      } 
      public function PlatParCategorie() { 

        $today = date("y/m/d");
        $this->load->model('Plat');
        $categorie = $this->input->get('categorie');
        $data['plats']=$this->Plat->platDuJourParCategorie($categorie, $today);
        $data['page']="ListeDesPlats";
        $this->load->view('accueil.php',$data); 
         
      } 
      public function rechercher() { 

        $this->load->model('Plat');
        $categorie = $this->input->get('categorie');
        $data['plats']=$this->Plat->rechercher($categorie);
        $data['page']="ListeDesPlats";
        $this->load->view('accueil.php',$data); 
         
      } 
   } 
?>