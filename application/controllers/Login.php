<?php
	class Login extends CI_Controller{
		public function __construct(){
			parent::__construct();
			$this->load->helper('form');
			$this->load->helper('url');
			$this->load->library('form_validation');
			$this->load->model('Serveur_Model');
			// $this->load->model('Plat');
		}
		public function index() { 
			// $query = $this->db->get("stud"); 
			// $data['records'] = $query->result(); 
			   
			$this->load->helper('url'); 
			$this->load->view('login'); 
		} 
		public function validation_login(){
			$data=array(
				'nom' =>$this->input->post('nom'),
				'password' =>$this->input->post('password')
			);
			$result = $this->Serveur_Model->checkLogin($data);

			if($result == TRUE){
				$this->load->model('Plat');				
				$data['plats']=$this->Plat->selectPlat();
				$this->load->view('accueil',$data);
			}
			else{
				$this->load->view('error');
			}
		}
	}
?>

