create database food;
use food;


create table Personne(
    idPersonne int primary key AUTO_INCREMENT,
    nom varchar(20),
    types varchar(20),
    mdp varchar(20)
);

create table Categorie(
    idCategorie int primary key AUTO_INCREMENT,
    nom varchar(20)
);

create table Plat(
    idPlat int primary key AUTO_INCREMENT,
    nom varchar(20),
    idCategorie int,
    prix float,
    photo varchar(20),
    foreign key (idCategorie) references Categorie(idCategorie)
);

create table Commande(
    idCommande int primary key AUTO_INCREMENT,
    idPlat int,
    nombre float,
    foreign key (idPlat) references Plat(idPlat)
);

create table Tables(
    idTables int primary key AUTO_INCREMENT,
    numero float
);

create table Facture(
    idFacture int primary key AUTO_INCREMENT,
    date date,
    idCommande int,
    somme float,
    idTables int,
    foreign key (idCommande) references Commande(idCommande),
    foreign key (idTables) references Tables(idTables)
);


insert into Personne values (null,'Jean','admin','0000');
insert into Personne values (null,'Mahery','Serveur','1111');
insert into Personne values (null,'Noum','Serveur','2222');


insert into Categorie values (null,'Pate');
insert into Categorie values (null,'Riz');
insert into Categorie values (null,'Viande');

insert into plat values (null,'Mine-Sao',1,'5000','CC.jpg');
insert into plat values (null,'Soupe van tan','1','6000','soupe vatane.jpg');

insert into plat values (null,'tsock',2,'5000','TSOCK.jpg');
insert into plat values (null,'Bol renverse',2,'6000','reverse.jpg');

insert into plat values (null,'Steak',3,'5000','steak.jpg');
insert into plat values (null,'cote pane',3,'5000','Porc pan.jpg');


